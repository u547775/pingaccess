def FAILED_STAGE
def BUILD_NUMBER

pipeline 
 {
    agent {label 'PA_node' }
    
	tools
	{
		jdk 'JAVA_HOME'
	} 
	options
	{
		buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
		
	}
	
	stages
	{
	    
	    stage('code checkout')
		{
		 steps
			{
			 script{
			     
			git([url: 'https://u547775@bitbucket.org/u547775/pingaccess.git', branch: 'master', credentialsId: 'b2fe4101-0710-44d6-af8a-0c4f8c1f4c92'])
			  }
             }			  
		}
	         
        
        stage('PingAccess Admin')
		{
		  agent {label 'PA_node' }
	       steps
		   {
		       sh "java -version"
		       sh "whoami"
			   sh "sudo chmod 777 /opt"
			   sh "echo taking backup of previous file"
               sh "sudo cp -R /opt/ping/admin/pingaccess-5.3.2/ /opt/Ping_backup/PAadmin_bkp/"
			   sh "sudo chown -R feduser:bpost /opt/ping"
			   sh "echo Deleting previous version"
			   sh "sudo rm -rfv /opt/ping/admin/"
			   sh "sudo unlink /opt/ping/current-admin/pingaccess"
			   sh "echo Deleting previous current version"
			   sh "sudo rm -rfv /opt/ping/current-admin"
			   sh "sudo mkdir /opt/ping/admin"
			   sh "sudo mkdir /opt/ping/current-admin"
			   sh "echo Copying admin zip file"
		       sh "sudo cp -R /var/lib/Jenkins/workspace/PingAccess/pingaccess-5.3.2.zip /opt/ping/admin"
			   sh "sudo chown -R feduser:bpost /opt/ping"
			   sh "echo Unzip PingAccess admin zip file"
			   sh "sudo unzip /opt/ping/admin/pingaccess-5.3.2.zip -d  /opt/ping/admin"
			   sh "sudo ln -s /opt/ping/admin/pingaccess-5.3.2 /opt/ping/current-admin/pingaccess"
			   sh "sudo cp -R /opt/Ping_backup/PAadmin_bkp/pingaccess-5.3.2/conf/run.properties  /opt/ping/admin/pingaccess-5.3.2/conf/"
			   sh "echo Running PingAccess admin services"
			   sh "sudo service pingaccess start"
			   sh "echo Service check for PingAccess admin"
			   sh "ps -ef |grep java"
	       }
         } 	

         stage('PingAccess Engine')
		{
		  agent {label 'PA_node' }
	       steps
		   {
		       sh "java -version"
		       sh "whoami"
			   sh "sudo chmod 777 /opt"
			   sh "echo taking backup of previous file"
               sh "sudo cp -R /opt/ping/engine/pingaccess-5.3.2/ /opt/Ping_backup/PAengine_bkp/"
			   sh "sudo chown -R feduser:bpost /opt/ping"
			   sh "echo Deleting previous version"
			   sh "sudo rm -rfv /opt/ping/engine/"
			   sh "sudo unlink /opt/ping/current-engine/pingaccess"
			   sh "echo Deleting previous current version"
			   sh "sudo rm -rfv /opt/ping/current-engine"
			   sh "sudo mkdir /opt/ping/engine"
			   sh "sudo mkdir /opt/ping/current-engine"
			   sh "echo Copying admin zip file"
		       sh "sudo cp -R /var/lib/Jenkins/workspace/PingAccess/pingaccess-5.3.2.zip /opt/ping/engine"
			   sh "sudo chown -R feduser:bpost /opt/ping"
			   sh "echo Unzip PingAccess admin zip file"
			   sh "sudo unzip /opt/ping/engine/pingaccess-5.3.2.zip -d  /opt/ping/engine"
			   sh "sudo ln -s /opt/ping/engine/pingaccess-5.3.2 /opt/ping/current-engine/pingaccess"
			   sh "sudo cp -R /opt/Ping_backup/PAengine_bkp/pingaccess-5.3.2/conf/run.properties  /opt/ping/engine/pingaccess-5.3.2/conf/"
			   //sh "echo Running PingAccess engine services"
			   //sh "sudo service pingaccess start"
			   //sh "echo Service check for PingAccess admin"
			   //sh "ps -ef |grep java"
	       }
        } 
            
        }
     } 